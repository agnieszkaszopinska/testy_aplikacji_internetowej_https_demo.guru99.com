# Agnieszka Szopińska 

## About me
I have been passionate about the IT industry for a short time. I decided to delve deeper into the subject by gaining the necessary knowledge during an intense Software Tester course. I have experience running several projects simultaneously. As a result of highly developed work organization skills, I know I can definitely handle documenting and reporting test results. I am inquisitive and meticulous, which will certainly pay off when reporting and handling defects.
<center>

![zdjęcie](/uploads/cd97aeaf339afc4190a0c3f3a99b4df7/zdjęcie.jpg)

## Software Development Academy Course
I had the pleasure of attending the "Software Tester" course organized by Software Development Academy. Through 105 hours of class time and many hours spent on independent work, I gained knowledge in the following selected topics:
<center>

* Introduction to testing (general concepts)
* Test design techniques (blackbox, whitebox)
* Risk-based testing (design and product risk; methods: PRisMa, FTA, QFD)
* Apps vs websites, SPA, MPA
* Exploratory testing
* Database Basics, MySQL Shel

[tester_oprogramowania.pdf](/uploads/9624c50a9fc1dc832d5a724f10cd5f34/tester_oprogramowania.pdf)

</center>

## SCRUM Course
I will also become proficient in agile projects thanks to the introductory class on Scrum methodology.

[scrum.pdf](/uploads/f0ab3fdcd993a09b6e06a1a1d1c63104/scrum.pdf)

</center>

## Assignments I completed during the course:
<center>

* Database basics
* Basics of programming
* Test design techniques
* Testing classes
* Selenium WebDriver
* BDD technique
* Final project

</center>

## My projects:

* [Test Cases in TestRail ](Test Cases in TestRail) for website [Demo99](https://demo.guru99.com/)

* [Exploratory testing](Exploratory testing) for website [Demo99](https://demo.guru99.com/)

* [Bug reporting in Jira](Bug reporting in Jira) for website [Demo99](https://demo.guru99.com/)

* [Exploratory testing](Exploratory testing) for website [OSM Koło](https://www.osmkolo.pl/)

</center>

## Interests:

* Member and Chairwoman of the Audit Committee of the Partisan Association "Gryf Pomorski" squad "Cis Męcikał"
* Traditional photography - especially travel photography
* Sports - squash, gym
* Healthy lifestyle

<center>

## Contact me:

Email: agnieszkaszopinska.pracait@gmail.com

Linkedin: [Agnieszka Szopińska](https://www.linkedin.com/in/agnieszka-szopińska/)
